# rushhour.py - Rush Hour Domain dependent code.
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC


##### Class Rush Hour Board #####
# Represents a Rush hour problem state and
# offers methods for generating new states.
#        
class Board:

    ### BFS constructor ###
    # [purpose]: Creates a new Board object.
    # [input]: size - board size (board will be size*size),
    # action - gameplay action that generated this board.
    # [output]: void.
    def __init__(self, size, action):
        self.size = int(size)
        self.matrix = []
        self.action = action
        self.actionList = []
        self.lvl = 1
        self.weight = 1

    ### intertLine ###
    # [purpose]: Insert lines into the Board. Usefull when reading
    # the board from a file.
    # [input]: line - list of characters representing
    # a line of the board.
    # [output]: void.
    def insertLine(self,line):
        self.matrix+=line

    ### __str__ ###
    # Redefinition of __str__() method to
    # convert the board into a string. 
    def __str__(self):
        chars=''
        size=self.size
        for i in range(size):
            for j in range(size):
                chars += self.matrix[i*size+j]
            chars+="\n"
        return chars

    ### heuristic0 ###
    # [purpose]: Blocking cars heuristic.
    # [input]: none.
    # [output]: Estimated cost to reach the goal = Number of cars
    # blocking the exit in this board.
    def heuristic0(self):
        count = 0
        size=self.size
        for m in range(size):
            if self.matrix[2*size + m] == 'R': ## Number of cars blocking the exit heuristic
                n = m
                break
        n = n+2
        while n < size:
            if self.matrix[2*size + n] != '-':
                count+=1
            n+=1
                
        return count

    ### heuristic1 ###
    # [purpose]: Cars Blocking the blocking cars heuristic.
    # [input]: none.
    # [output]: Estimated cost to reach the goal = Number of cars
    # blocking the exit + number of blocking cars that cannot move
    # at least 1 step up or down, in this board.
    def heuristic1(self):
        count = 0
        size=self.size

        blockingCars = []

        for m in range(size): 
            if self.matrix[2*size + m] == 'R':
                n = m
                break
        n = n+2
        while n < size:
            if self.matrix[2*size + n] != '-': ## Number of cars blocking the exit heuristic
                
                count+=1 # Count blocking car
                carId = self.matrix[2*size + n]
                
                combUp = 1 #line above solution line
                combDn = 3 #line below solution line
                
                while True: #Adjust the comb
                    if self.matrix[combUp*size + n] == carId:
                        combUp-=1
                        continue
                    if self.matrix[combDn*size + n] == carId:
                        combDn+=1
                        continue
                    break
                
                if (self.matrix[combUp*size + n] != '-' or combUp < 0) and (self.matrix[combDn*size + n] != '-' or combDn > 5):

                    # Account only one of the Blocking-Blocking cars. First checkout Up then Dn.
                    # If the Blocking car is blocked by the borded of the board, no count is done. 
                    #    
                    if (not combUp < 0) and (not blockingCars.__contains__(self.matrix[combUp*size + n])):
                        count+=1# Blocking-Blocking car is new. Add cost to heuristic.
                        blockingCars.append(self.matrix[combUp*size + n])
                        
                    elif (not combDn > 5) and (not blockingCars.__contains__(self.matrix[combDn*size + n])):
                        count+=1# Blocking-Blocking car is new. Add cost to heuristic.
                        blockingCars.append(self.matrix[combDn*size + n])
                        

            n+=1

        return count

    ### heuristic2 ###
    # [purpose]: Cars Blocking the blocking cars heuristic 2.
    # [input]: none.
    # [output]: Estimated cost to reach the goal = Number of cars
    # blocking the exit + number of blocking cars that cannot
    # move away from the exit path.
    def heuristic2(self):
        count = 0
        size=self.size
        
        blockingCars = []
        
        for m in range(size): 
            if self.matrix[2*size + m] == 'R':
                n = m
                break
        n = n+2
        while n < size:
            if self.matrix[2*size + n] != '-': ## Number of cars blocking the exit heuristic
                count+=1

                carId = self.matrix[2*size + n]
                combUp = 1 #line above solution line
                combDn = 3 #line below solution line
                
                while True: #Adjust the comb
                    if self.matrix[combUp*size + n] == carId:
                        combUp-=1
                        continue
                    if self.matrix[combDn*size + n] == carId:
                        combDn+=1
                        continue
                    break

                carsize = combDn - combUp - 1

                if carsize == 3: #Big cars can only move down
                    for k in range(6-combDn):
                        if self.matrix[(combDn+k)*size + n] != '-':# Car cannot move away from the path between the red car and the exit point
                            if not blockingCars.__contains__(self.matrix[(combDn+k)*size + n]):
                                count+=1# Blocking-Blocking car is new. Add cost to heuristic.
                                blockingCars.append(self.matrix[(combDn+k)*size + n])
                            break
                        
                else: # Small cars
                    candidateBlockingCarUp = '*'
                    candidateBlockingCarDn = '*'
                    
                    # Check availability to move up
                    #
                    if self.matrix[size + n] == '-' or self.matrix[size + n] == carId:
                        if self.matrix[n] == '-':
                            n+=1
                            continue # Car is not blocked
                        else:
                            # Blocking-Blocking car on line 0
                            candidateBlockingCarUp = self.matrix[n]
                    else:
                        # Blocking-Blocking car on line 1
                        candidateBlockingCarUp = self.matrix[size + n]

                    # Check availability to move down
                    #   
                    if self.matrix[combDn*size + n] == '-' or self.matrix[combDn*size + n] == carId:
                        
                        if self.matrix[(combDn+1)*size + n] == '-':
                            n+=1
                            continue # Car is not blocked
                        else:
                            # Blocking-Blocking car on line combDn+1
                            BlockingCarDn = self.matrix[(combDn+1)*size + n]
                    else:
                        # Blocking-Blocking car on line combDn
                        BlockingCarDn = self.matrix[combDn*size + n]
                        
                    # Check if the Blocking-Blocking cars are not repeated
                    # Also, only one Blocking-Blocking car (Up or Dn) is added per car
                    #
                    if candidateBlockingCarUp != '*' and not blockingCars.__contains__(candidateBlockingCarUp):
                        count+=1# Blocking-Blocking car is a new counting car. Add cost to heuristic.
                        blockingCars.append(candidateBlockingCarUp)
                    elif candidateBlockingCarDn != '*' and not blockingCars.__contains__(candidateBlockingCarDn):
                        count+=1# Blocking-Blocking car is a new counting car. Add cost to heuristic.
                        blockingCars.append(candidateBlockingCarDn)
            n+=1

        return count

    ### goalTest ###
    # [purpose]: Test if this board is the goal state.
    # [input]: none.
    # [output]: True if this board is the goal state, False otherwise.         
    def goalTest(self):

        size=self.size
        if self.matrix[2*size+5] == 'R':
            return True
        else:
            return False

    ### copyState ###
    # [purpose]: Copies atributes from onde state to the other
    # [input]: destination state, origin state
    # [output]: void.
    def copyState(self,origin):
        self.actionList = origin.actionList
        self.weight = origin.weight
        self.lvl = origin.lvl
        self.action = origin.action
        
    ### __cloneChild ###
    # [purpose]: Create a child equal to this board.
    # [input]: action - The gameplay action that generated this child.
    # [output]: child - The generated child.  
    def __cloneChild(self,action):

        child = Board(self.size, action)
        child.actionList += self.actionList
        child.actionList.append(action)
        child.matrix += self.matrix
        child.lvl = self.lvl+1
        return child

    ### successorFunction ###
    # [purpose]: Generates all possible child nodes from this board. Each
    # child results from a legal move applied to a car. Moves that would
    # generate again the board that generated this board are avoided.
    # A complete repeated node check must be implemented in the calling
    # algorithm if such feature is desired.
    # [input]: none.
    # [output]: generated child nodes.      
    def successorFunction(self):
        retlist = []
        self.__tryLeftMoves(retlist)
        self.__tryRightMoves(retlist)
        self.__tryUpMoves(retlist)
        self.__tryDownMoves(retlist)
        
        return retlist

    ### __repeatedMove ###
    # [purpose]: Returns True if newAction is the oposite
    # action of oldAction. 
    # [input]: newAction, oldAction.
    # [output]: True is the input actions are oposite.
    def __repeatedMove(self,newAction,oldAction):

        #Check if it this the same car
        if newAction[0] == oldAction[0]:
            if newAction[1]=='L' and oldAction[1]=='R':
                return True
            elif newAction[1]=='R' and oldAction[1]=='L':
                return True
            elif newAction[1]=='U' and oldAction[1]=='D':
                return True
            elif newAction[1]=='D' and oldAction[1]=='U':
                return True
        return False

    ### __tryLeftMoves ###
    # [purpose]: Checks and generates all possible legal car moves to the left. 
    # [input]: retlist - List to which newly generated child nodes are added.
    # [output]: (modification of retlist)
    def __tryLeftMoves(self, retlist):
        ### row-wise search for cars: Try to move Left###        
        i=0
        j=0
        size = self.size
        while i < size:
            while j < size:
                if self.matrix[i*size+j] == '-':
                    j+=1
                    continue
                else: #candidate for row car
                    if j == (size-1):#surely not a row car
                        j+=1
                        continue
                    carId = self.matrix[i*size+j]
                    carSize = 1
                    k=j+1
                    while k < size and self.matrix[i*size+k] == carId:
                        carSize+=1
                        k+=1
                    if carSize < 2:#not a row car
                        j+=1
                        continue

                    assert (carSize <= 3)#too big car
                   
                    if not self.__repeatedMove(carId+'R',self.action[0:2]):
                        for mov in [1,2,3,4]:
                            if j-mov >= 0 and self.matrix[i*size + j-mov] == '-': #car not at left edge nor trying to move out of the board and moving to empty space

                                child = self.__cloneChild(carId+'L'+str(mov))
                                
                                for h in range(carSize):#move car
                                    child.matrix[i*size + j-mov+h] = child.matrix[i*size + j+h]
                                    
                                child.matrix[i*size + j-mov+carSize] = '-' #FIXME: old code for road block

                                if mov>1:
                                    for y in range(mov-1):
                                        child.matrix[i*size + j+(carSize-mov) + (y+1)] = '-'                                    
                                retlist.append(child)
                                
                            else:
                                break
                        j+=carSize-1 #go to next point of interest -1 because of default+1
                j+=1
            j=0
            i+=1

    ### __tryRightMoves ###
    # [purpose]: Checks and generates all possible legal car moves to the right. 
    # [input]: retlist - List to which newly generated child nodes are added.
    # [output]: (modification of retlist)
    def __tryRightMoves(self, retlist):
        ### row-wise search for cars: Try to move Right###        
        i=0
        j=0
        size = self.size
        while i < size:
            while j < size:
                if self.matrix[i*size + j] == '-':
                    j+=1
                    continue
                else:#candidate for row car
                    if j == (size-1):#surely not a row car
                        j+=1
                        continue
                    carId = self.matrix[i*size + j]
                    carSize = 1
                    k=j+1
                    while k < size and self.matrix[i*size + k] == carId:
                        carSize+=1
                        k+=1

                    if carSize < 2:#not a row car
                        j+=1
                        continue

                    assert(carSize <= 3)#too big car
               
                    if not self.__repeatedMove(carId+'L',self.action[0:2]):
                        for mov in [1,2,3,4]:
                    
                            if not j+mov+(carSize-1) >= size and self.matrix[i*size + j+mov+(carSize-1)] == '-': #empty space

                                child = self.__cloneChild(carId+'R'+str(mov))

                                for h in range(carSize):#move car
                                    child.matrix[i*size +j+mov+h] = child.matrix[i*size + j+h]
                                child.matrix[i*size + j+mov-1] = '-' #FIXME: old code for road block

                                if mov>1:
                                    for y in range(mov-1):
                                        child.matrix[i*size + j+y] = '-'    
                                retlist.append(child)
                            else:
                                break
                        j+=carSize-1
                j+=1
            j=0
            i+=1

    ### __tryUpMoves ###
    # [purpose]: Checks and generates all possible legal car moves up. 
    # [input]: retlist - List to which newly generated child nodes are added.
    # [output]: (modification of retlist)
    def __tryUpMoves(self, retlist):
        ### row-wise search for cars: Try to move Up###        
        m=0
        n=0
        size = self.size
        while m < size:
            while n < size:
                if self.matrix[n*size + m] == '-':
                    n+=1
                    continue
                else:#candidate for col car
                    if n == (size-1):#surely not a col car
                        n+=1
                        continue
                    carId = self.matrix[n*size + m]
                    carSize = 1
                    k=n+1
                    while k < size and self.matrix[k*size + m] == carId:
                        carSize+=1
                        k+=1
                    if carSize < 2:#not a col car
                        n+=1
                        continue

                    assert(carSize <= 3)#too big car

                    if not self.__repeatedMove(carId+'D',self.action[0:2]):
                        for mov in [1,2,3,4]:
                            if n-mov >= 0 and self.matrix[(n-mov)*size + m] == '-': #not at upper edge nor empty space

                                child = self.__cloneChild(carId+'U'+str(mov))
                                for v in range(carSize):
                                    child.matrix[(n-mov+v)*size + m] = child.matrix[(n+v)*size + m]
                                child.matrix[(n-mov+carSize)*size + m] = '-' #FIXME: old code for road block

                                if mov>1:
                                    for x in range(mov-1):
                                        child.matrix[(n+carSize-1-x)*size + m] = '-'
                                retlist.append(child)
                                
                            else:
                                break
                        n+=(carSize-1) #take one due to default+1
                n+=1
            n=0
            m+=1

    ### __tryDownMoves ###
    # [purpose]: Checks and generates all possible legal car moves down. 
    # [input]: retlist - List to which newly generated child nodes are added.
    # [output]: (modification of retlist)
    def __tryDownMoves(self, retlist):
        ### row-wise search for cars: Try to move Down###                         
        m=0
        n=0
        size=self.size
        while m < size:
            while n < size:
                if self.matrix[n*size + m] == '-':
                    n+=1
                    continue
                else:#candidate for col car
                    carId = self.matrix[n*size + m]
                    if n == (size-1):#surely not a col car
                        n+=1
                        continue
                    carId = self.matrix[n*size + m]
                    carSize = 1
                    k=n+1
                    while k < size and self.matrix[k*size + m] == carId:
                        carSize+=1
                        k+=1
                    if carSize < 2:#not a row car
                        n+=1
                        continue

                    assert(carSize <= 3)#too big car

                    if not self.__repeatedMove(carId+'U',self.action[0:2]):
                        for mov in [1,2,3,4]:
                            if n+carSize+mov-1 <= (size-1) and self.matrix[(n+carSize+mov-1)*size + m] == '-': #empty space and car can move
                                child = self.__cloneChild(carId+'D'+str(mov))        
                                for v in range(carSize):
                                    child.matrix[(n+mov+v)*size + m] = child.matrix[(n+v)*size + m]
                                    
                                child.matrix[(n+mov-1)*size + m] = '-' #FIXME: old code for road block

                                if mov>1:
                                    for x in range(mov-1):
                                        child.matrix[(n+x)*size + m] = '-'
                                retlist.append(child)
                            else:
                                break
                        #n+=(carSize-1+mov) FAILING MOV OPTIMIZATION
                        n+=carSize-1
                n+=1
            n=0
            m+=1
        return retlist
