# super.py - Script to make multiple main.py calls with diferent arguments
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC

from subprocess import call

#Super script for rushhour testing.
#Warning: Only works on windows.

##call(["C:\\Python33\python.exe","main.py", "Boards/Board0", "IDDFS"])
##call(["C:\\Python33\python.exe","main.py", "Boards/Board0", "BFS"])
##call(["C:\\Python33\python.exe","main.py", "Boards/Board0", "A*"])
##call(["C:\\Python33\python.exe","main.py", "Boards/Board0", "A*", "h1"])
##
##for i in [1,2,3,4,5,6,12,18,24,30]:
##    board = "Boards/test0"+str(i)
##    call(["C:\\Python33\python.exe","main.py", board, "IDDFS"])
##    call(["C:\\Python33\python.exe","main.py", board, "BFS"])
##    call(["C:\\Python33\python.exe","main.py", board, "A*"])
##    call(["C:\\Python33\python.exe","main.py", board, "A*", "h1"])

##call(["python3", "main.py", "Boards/Board0", "IDDFS"])
##call(["python3", "main.py", "Boards/Board0", "BFS"])
##call(["python3", "main.py", "Boards/Board0", "A*"])
##call(["python3", "main.py", "Boards/Board0", "A*", "h1"])
##call(["python3", "main.py", "Boards/Board0", "A*", "h2"])
##
##
for i in [1,2,3,4,5,6]:
    board = "Boards/test0"+str(i)
    call(["python3", "main.py", board, "IDDFS"])
    call(["python3", "main.py", board, "BFS"])
    call(["python3", "main.py", board, "A"])
    call(["python3", "main.py", board, "A", "h1"])
    call(["python3", "main.py", board, "A", "h2"])

board = "Boards/test18"
#call(["python3", "main.py", board, "IDDFS"])
call(["python3", "main.py", board, "BFS"])
call(["python3", "main.py", board, "A"])
call(["python3", "main.py", board, "A", "h1"])
call(["python3", "main.py", board, "A", "h2"])
    
for i in [30]:
    board = "Boards/test"+str(i)
##    call(["main.py", board, "IDDFS"])
    call(["python3", "main.py", board, "BFS"])
    call(["python3", "main.py", board, "A"])
    call(["python3", "main.py", board, "A", "h1"])
    call(["python3", "main.py", board, "A", "h2"])
##
#for i in range(10):
#    board = "levels/level"+str(i)+".txt"
#    call(["python3", "main.py", board, "IDDFS"])
##    call(["python3", "main.py", board, "BFS"])
##    call(["python3", "main.py", board, "A"])
##    call(["python3", "main.py", board, "A", "h1"])
##    call(["python3", "main.py", board, "A", "h2"])
###
#board = "levels/level18.txt"
##call(["python3", "main.py", board, "IDDFS"])
##call(["python3", "main.py", board, "BFS"])
##call(["python3", "main.py", board, "A"])
##call(["python3", "main.py", board, "A", "h1"])
##call(["python3", "main.py", board, "A", "h2"])
##    
#for i in range(29):
#    board = "levels/level"+str(i+10)+".txt"
###    call(["main.py", board, "IDDFS"])
#    call(["python3", "main.py", board, "BFS"])
#    call(["python3", "main.py", board, "A"])
#    call(["python3", "main.py", board, "A", "h1"])
#    call(["python3", "main.py", board, "A", "h2"])
#
#
#board = "levels/level39.txt"
#call(["python3", "main.py", board, "BFS"])
#call(["python3", "main.py", board, "A"])
#call(["python3", "main.py", board, "A", "h1"])
#call(["python3", "main.py", board, "A", "h2"])
