# utils.py - Print utilities
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC


### printActionTree ###
# [purpose]: Prints action tree of a given node.
# [input]: node.
# [output]: string with the printed result.
def printActionTree(node):

    solseq = ''
    for action in node.actionList:
        solseq += '\n' + action
    solseq = '%d'%(len(node.actionList)) + solseq
    return solseq

### countActions ###
# [purpose]: Counts the number of actions that lead to a node.
# [input]: node.
# [output]: number of actions that lead to a node.
def countActions(node):
    return len(node.actionList)


### printResults ###
# [purpose]: Prints tuple returned from generalSearch with execution info.
# [input]: info, verbose flag.
# [output]: printed results.
def printResults(stats, verbose, fo):

    if fo=='console':
        if stats[0] == 1:
            if verbose:
                print("Found solution.\nNumber of actions: {0}".format(countActions(stats[1])))
                print('Number of iterations: %d'%(stats[2]))
                print('Maximum number of nodes in memory: {0}'.format(stats[3]+stats[4]))
                print('\t of which openList:{0}'.format(stats[3]))
                print('\t of which closedList:{0}'.format(stats[4]))
                print('Total generated nodes: {0}'.format(stats[5]))

            else:
                print("Found Solution.\nNumber of actions: {0}".format(countActions(stats[1])))
        else:
            if verbose:
                print("Did not find solution")
                print('Number of iterations: %d'%(stats[2]))
                print('Maximum number of nodes in memory: {0}'.format(stats[3]+stats[4]))
                print('\t of which openList:{0}'.format(stats[3]))
                print('\t of which closedList:{0}'.format(stats[4]))
                print('Total generated nodes: {0}'.format(stats[5]))
            else:
                print("Did not find solution")
    else:
        if stats[0] == 1:
            if verbose:
                print("Found solution.\nNumber of actions: {0}".format(countActions(stats[1])), file = fo)
                print('Number of iterations: %d'%(stats[2]), file = fo)
                print('Maximum number of nodes in memory: {0}'.format(stats[3]+stats[4]), file = fo)
                print('\t of which openList:{0}'.format(stats[3]), file = fo)
                print('\t of which closedList:{0}'.format(stats[4]), file = fo)
                print('Total generated nodes: {0}'.format(stats[5]), file = fo)

            else:
                print("Found Solution.\nNumber of actions: {0}".format(countActions(stats[1])), file = fo)
        else:
            if verbose:
                print("Did not find solution", file = fo)
                print('Number of iterations: %d'%(stats[2]), file = fo)
                print('Maximum number of nodes in memory: {0}'.format(stats[3]+stats[4]), file = fo)
                print('\t of which openList:{0}'.format(stats[3]), file = fo)
                print('\t of which closedList:{0}'.format(stats[4]), file = fo)
                print('Total generated nodes: {0}'.format(stats[5]), file = fo)
            else:
                print("Did not find solution", file = fo)
