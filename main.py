# main.py - Main script for solving rush hour puzzles.
#
# Usage: main.py INPUT_BOARD ALGORITHM [HEURISTIC]
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC

import rushhour # RushHour domain dependent code
import search # Search algorithms (Domain independent code)
import utils # Utilities from printing results

import time # lib required for measuring time
import sys # lib required for setting the arguments

# Uncomment this line to run the script directly
# without having to provide arguments:
#
#sys.argv = ['main.py','levels/level15.txt','A','h1']

verbose = True

if(len(sys.argv)<3):
    print('Usage: main.py INPUT_BOARD ALGORITHM [HEURISTIC]')
    print('Algorithms: BFS, IDDFS, IDDFS_R, A')
    print('Heuristics(A*): h0(default), h1')
    sys.exit()
    
fi = open(sys.argv[1])
line = fi.readline()
board = rushhour.Board(len(line)-1,'INIT')
board.insertLine(list(line.split('\n')[0]))

print('Reading file...')

while True: #read file
    line = fi.readline()
    if len(line) == 0: #EOF
        break
    board.insertLine(list(line.split('\n')[0]))
    
fi.close()

# Run Chosen Algorithm:

if(sys.argv[2]=='BFS'):
    BFS = search.BFS(board)
    print('Running BFS...')
    start=time.time()
    stats = search.generalSearch(board, BFS, verbose)
    end=time.time()
    print('Time: %d ms'%((end-start)*1000))
    seqString=utils.printActionTree(stats[1])
    
elif(sys.argv[2]=='IDDFS'):
    IDDFS = search.IDDFS(1, board)
    print('Running IDDFS...')
    start=time.time()
    stats = search.generalSearch(board, IDDFS, verbose)
    end=time.time()
    print('Time: %d ms'%((end-start)*1000))
    seqString=utils.printActionTree(stats[1])

elif(sys.argv[2]=='IDDFS_R'):
    IDDFS_FullRepeat = search.IDDFS_FullRepeat(2, board)
    print('Running IDDFS_R...')
    start=time.time()
    stats = search.generalSearch(board, IDDFS_FullRepeat, verbose)
    end=time.time()
    print('Time: %d ms'%((end-start)*1000))
    seqString=utils.printActionTree(stats[1])
    
elif(sys.argv[2]=='A'):

    if(len(sys.argv)>=4):
        if(sys.argv[3]=='h1'):
            board.weight = board.lvl + board.heuristic1()
            Astar = search.Astar(board, 1)
        elif(sys.argv[3]=='h2'):
            board.weight = board.lvl + board.heuristic2()
            Astar = search.Astar(board, 2)
        else:#default is h0
            board.weight = board.lvl + board.heuristic0()
            Astar = search.Astar(board, 0)
    else:#default is h0
        board.weight = board.lvl + board.heuristic0()
        Astar = search.Astar(board, 0)

    print('Running A*...')
    start=time.time()
    stats = search.generalSearch(board, Astar, verbose)
    end=time.time()
    print('Time: %d ms'%((end-start)*1000))
    seqString=utils.printActionTree(stats[1])

else:
    print('[ERROR]: Invalid algorithm')
    print('Usage: main.py INPUT_BOARD ALGORITHM [HEURISTIC]')
    print('Algorithms: BFS, IDDFS, IDDFS_R, A')
    print('Heuristics(A*): h0(default), h1')
    sys.exit()


# Print result sequence to file:
if len(sys.argv) == 4:
    fo = open(sys.argv[1]+'_'+sys.argv[2]+sys.argv[3]+'.sol','w')
else:
    fo = open(sys.argv[1]+'_'+sys.argv[2]+'.sol','w')
    
print(seqString,end='',file=fo)

# Report statistics to file
if len(sys.argv) == 4:
    fo_stats = open(sys.argv[1]+'_'+sys.argv[2]+sys.argv[3]+'_stats'+'.sol','w')
else:
    fo_stats = open(sys.argv[1]+'_'+sys.argv[2]+'_stats'+'.sol','w')
    
print('Running '+sys.argv[2], file = fo_stats)
print('Time: %d ms'%((end-start)*1000), file = fo_stats)
# utils.printResults(stats,verbose, fo_stats)
utils.printResults(stats,verbose, 'console')


fo.close()
